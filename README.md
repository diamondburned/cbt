# cbt

cbt, or Chat Back-end Terminal, is a TUI chat application which aims to support multiple services packed in one clean TUI.

## Progress

- [ ] Back-end interface
	- [ ] Discord
	- [ ] Slack
	- [ ] IRC
	- [ ] Think of more
- [ ] Terminal front-end
	- [ ] [gowid](https://github.com/gcla/gowid)
	- [ ] Image support
