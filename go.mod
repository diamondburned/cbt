module gitlab.com/diamondburned/cbt

go 1.12

require (
	github.com/bwmarrin/discordgo v0.19.0
	github.com/gcla/gowid v1.0.0
)
