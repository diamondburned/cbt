package backend

// SettingsType is a constant that determines
// how
type SettingsType int

const (
	// None disables Settings. The front-end
	// will not touch the Settings method.
	None SettingsType = iota

	// PrivateKeyValue enables Settings for
	// internal use, but the frontend will
	// not allow users to change it.
	PrivateKeyValue

	// PublicKeyValue enables Settings and
	// allows users to change it.
	PublicKeyValue
)
