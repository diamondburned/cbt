package discord

import (
	"time"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/diamondburned/cbt/backend"
)

// ConvertMessageCreate adapts a Discord message into a Backend message.
// TODO: markdown, embed, nickname, color
func (d *Discord) ConvertMessageCreate(m *discordgo.Message) *backend.Message {
	t, err := m.Timestamp.Parse()
	if err != nil {
		t = time.Now()
	}

	return &backend.Message{
		ID:      m.ID,
		Type:    backend.Create,
		Content: m.Content,
		Channel: &backend.Channel{
			ID: m.ChannelID,
			Server: &backend.Server{
				ID: m.GuildID,
			},
		},
		Author: &backend.User{
			Username: m.Author.Username,
		},
		Meta: &backend.MessageMeta{
			Timestamp: t,
		},
	}
}

// ConvertMessageUpdate adapts a Discord message edit into a Backend message.
// TODO: markdown, embed, nickname, color
func (d *Discord) ConvertMessageUpdate(m *discordgo.MessageUpdate) *backend.Message {
	t, err := m.EditedTimestamp.Parse()
	if err != nil {
		t, err = m.Timestamp.Parse()
		if err != nil {
			t = time.Now()
		}
	}

	return &backend.Message{
		ID:      m.ID,
		Type:    backend.Edit,
		Content: m.Content,
		Channel: &backend.Channel{
			ID: m.ChannelID,
			Server: &backend.Server{
				ID: m.GuildID,
			},
		},
		Meta: &backend.MessageMeta{
			Timestamp: t,
		},
	}
}

// ConvertMessageDelete adapts a Discord message delete into a Backend message.
func (d *Discord) ConvertMessageDelete(m *discordgo.MessageDelete) *backend.Message {
	return &backend.Message{
		ID:   m.ID,
		Type: backend.Delete,
		Channel: &backend.Channel{
			ID: m.ChannelID,
			Server: &backend.Server{
				ID: m.GuildID,
			},
		},
	}
}
