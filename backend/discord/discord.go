// Package discord implements backend for discordgo.
package discord

import (
	"errors"
	"sort"
	"sync"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/diamondburned/cbt/backend"
)

// Asserts that Discord must satisfy the Backend interface.
var _ backend.Backend = (*Discord)(nil)

// Discord is the structure which satisfies the Backend
// interface.
type Discord struct {
	Session *discordgo.Session
	Channel *backend.Channel

	// TODO
	settings map[string]string

	// This should NEVER be closed if Channel is not nil.
	msgCh chan *backend.Message

	// This mutex prevents incoming messages from sending
	// into a closed channel if there's one.
	mut sync.Mutex
}

var (
	// ErrChannelNotFound is returned when the channel can't
	// be found. This could be that the server can't be found,
	// the channel does not exist, or other reasons.
	ErrChannelNotFound = errors.New("channel not found")
)

// Authenticate takes in one token only. As a result,
// this backend's Capability.Authenticating would have
// []string{"Token"}
func (d *Discord) Authenticate(args ...string) error {
	s, err := discordgo.New(args[0])
	if err != nil {
		return err
	}

	s.StateEnabled = true
	s.SyncEvents = true

	s.AddHandler(func(s *discordgo.Session, m *discordgo.MessageCreate) {
		d.wrapCheck(m.Message, func() {
			d.msgCh <- d.ConvertMessageCreate(m.Message)
		})
	})

	s.AddHandler(func(s *discordgo.Session, m *discordgo.MessageUpdate) {
		d.wrapCheck(m.Message, func() {
			d.msgCh <- d.ConvertMessageUpdate(m)
		})
	})

	s.AddHandler(func(s *discordgo.Session, m *discordgo.MessageDelete) {
		d.wrapCheck(m.Message, func() {
			d.msgCh <- d.ConvertMessageDelete(m)
		})
	})

	if err := s.Open(); err != nil {
		return err
	}

	d.Session = s

	done := make(chan struct{})
	s.AddHandlerOnce(func(d *discordgo.Session, r *discordgo.Ready) {
		done <- struct{}{}
	})

	<-done

	return nil
}

// GetServers gets all the servers
func (d *Discord) GetServers() ([]*backend.Server, error) {
	servers := make([]*backend.Server, 0, len(d.Session.State.Guilds)+1)

	dm := &backend.Server{
		ID: "",
		Channels: make(
			[]*backend.Channel, 0, len(d.Session.State.PrivateChannels),
		),
	}

	for _, c := range d.Session.State.PrivateChannels {
		dm.Channels = append(dm.Channels, &backend.Channel{
			ID:        c.ID,
			Reference: c,
			Server:    dm,
		})
	}

	for _, g := range d.Session.State.Guilds {
		s := &backend.Server{
			ID:       g.ID,
			Channels: make([]*backend.Channel, 0, len(g.Channels)),
		}

		for _, c := range g.Channels {
			s.Channels = append(s.Channels, &backend.Channel{
				ID:        c.ID,
				Reference: c,
				Server:    s,
			})
		}
	}

	return servers, nil
}

// GetCapability returns this service's capabilities.
func (d *Discord) GetCapability() backend.Capability {
	return backend.Capability{
		Authenticating:    []string{"Token"},
		MessageCapability: backend.Create | backend.Edit | backend.Delete,
		SettingsType:      backend.PublicKeyValue,
	}
}

// Join joins a Discord channel
func (d *Discord) Join(ch *backend.Channel) (<-chan *backend.Message, error) {
	d.mut.Lock()
	defer d.mut.Unlock()

	// Clean up the channel if it's still open
	select {
	case _, ok := <-d.msgCh:
		if ok {
			// if d.Channel = nil, AddHandler will refuse all incoming
			// messages.
			d.Channel = nil
			close(d.msgCh)
		}
	default:
	}

	c, err := d.Session.State.Channel(ch.ID)
	if err != nil {
		return nil, err
	}

	d.Channel = &backend.Channel{
		Reference: c,
		ID:        c.ID,
		Server: &backend.Server{
			ID: c.GuildID,
		},
	}

	// Make a buffered channel. The buffered length is equal to the number
	// of old messages to fetch for the backlog.
	d.msgCh = make(chan *backend.Message, 25)

	msgs, err := d.Session.ChannelMessages(c.ID, 25, "", "", "")
	if err != nil {
		return nil, err
	}

	sort.Slice(msgs, func(i, j int) bool {
		return msgs[i].ID < msgs[j].ID
	})

	for _, m := range msgs {
		d.msgCh <- d.ConvertMessageCreate(m)
	}

	return d.msgCh, nil
}

// Send sends a message to the current channel.
func (d *Discord) Send(content string) error {
	if d.Channel == nil {
		return ErrChannelNotFound
	}

	_, err := d.Session.ChannelMessageSend(d.Channel.ID, content)
	return err
}

func (d *Discord) filterMessage(m *discordgo.Message) bool {
	if d.Channel == nil {
		return false
	}

	if m.ChannelID == d.Channel.ID {
		return true
	}

	return false
}

func (d *Discord) wrapCheck(m *discordgo.Message, f func()) {
	if !d.filterMessage(m) {
		return
	}

	d.mut.Lock()
	defer d.mut.Unlock()

	f()
}
