// Package backend provides an interface for interactions
// between the frontend TUI and the backend services such
// as Discord, Slack, IRC, etc.
package backend

import (
	"time"

	"github.com/gcla/gowid/widgets/text"
)

// Backend describes an interface for a chat service backend.
type Backend interface {
	// Authenticate tries to sign into the service. This
	// is applicable on backends such as Discord. Other
	// backends such as IRC could have this method return
	// nil.
	// The array fed into this structure will always have
	// the same length as Capability.Authenticating.
	Authenticate(...string) error

	// GetServers is called when the frontend initiates,
	// asking for a list of servers to display.
	GetServers() ([]*Server, error)

	// Join should join a channel, ...string being the
	// arguments needed to join one. For Discord, this
	// could be Guild ID and Channel ID.
	Join(*Channel) (<-chan *Message, error)

	// Send sends the message. Services should keep track
	// of the channel to send the message to.
	Send(string) error

	// GetCapability implements a generalized
	// capability structure, allowing the TUI to determine
	// various factors in user interaction.
	GetCapability() Capability

	// Getters and setters for Settings, refer to settings.go
	GetSettings() (map[string]string, error)
	SetSettings(map[string]string) error
}

// Message is a generalized message structure. There is only
// one way to render the message in the front-end, thus the
// backend needs to satisfy this struct.
type Message struct {
	Reference interface{}

	// These fields aren't required if Type is not Create.
	// The ID will be used to match.
	//
	// For Edit, if any of these fields are provided except
	// Channel, it will be modified in the message as well.
	// A particular usecase for this would be to update the
	// Timestamp in Meta.
	Channel *Channel
	Author  *User
	Meta    *MessageMeta

	ID      string
	Type    MessageType
	Content string // not used if Type == Delete

	// Optional
	Formatted []text.ContentSegment

	// TODO: expand this
}

type MessageMeta struct {
	Timestamp time.Time
}

type User struct {
	Username string
	Nickname string
	Color    int
}

// Channel is a generalized channel structure.
type Channel struct {
	Reference interface{}

	// If a service does not implement a Server,
	// it should implement a default dummy server
	// variable for use.
	Server *Server

	// ID could be an ID, name, or anything the
	// service requires.
	ID string

	Meta *ChannelMeta
}

// ChannelMeta TODO
type ChannelMeta struct {
	// TODO: member list, etc
}

// Server is a generalized server structure. If a
// service does not implement this, it needs a dummy
// variable instead.
type Server struct {
	Reference interface{}

	// ID could be an address, ID or anything.
	ID string

	Meta ServerMeta

	// This is only used when the frontend asks for
	// a list of servers and channels.
	Channels []*Channel
}

// ServerMeta todo
type ServerMeta struct {
	// TODO: name, desc, etc
}
