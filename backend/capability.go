package backend

// Capability implemets a generalized capability
// structure, allowing the TUI to determine various
// factors in user interaction.
type Capability struct {
	// This field is dedicated to an array of key
	// names for authentication.
	// For example, if a backend requires a
	// Username and Password for
	// Backend.Authenticate(), it should have
	//
	//     []string{"Username", "Password"}
	//
	// for its Capability.Authenticating.
	Authenticating []string

	MessageCapability MessageType // bitwise
	SettingsType      SettingsType
}

// MessageType defines the message type. If used as a
// capability, this determines if a service is capable
// of editing or deleting messages, which affects both
// user interaction and how the client renders messages.
// If the service is not capable of editing or deleting,
// the client would not try to detect those events either.
type MessageType int

const (
	// Create defines basic message sent/received.
	Create MessageType = 1 << iota

	// Edit defines message editing. An example
	// of a service would be Discord.
	Edit

	// Delete determines message deleting. An
	// example would also be Discord.
	Delete
)
